var striptags = require('striptags');
 
var html =
    '<a href="https://example.com">' +
        'lorem ipsum <strong>dolor</strong> <em>sit</em> amet' +
    '</a>';
 
console.log(striptags(html));